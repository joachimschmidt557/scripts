PREFIX?=/usr/local

all: youtube-mp3 nope blue

install:
	install -Dm755 youtube-mp3 $(DESTDIR)$(PREFIX)/bin/youtube-mp3
	install -Dm755 nope $(DESTDIR)$(PREFIX)/bin/nope
	install -Dm755 blue $(DESTDIR)$(PREFIX)/bin/blue

uninstall:
	$(RM) $(DESTDIR)$(PREFIX)/bin/youtube-mp3
	$(RM) $(DESTDIR)$(PREFIX)/bin/nope
	$(RM) $(DESTDIR)$(PREFIX)/bin/blue

check:
	shellcheck youtube-mp3
	shellcheck nope
	shellcheck blue

.PHONY: all install uninstall check
