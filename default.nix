with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "my-scripts";

  src = ./.;
  # src = builtins.fetchGit {
  #   url = "https://github.com/joachimschmidt557/src";
  #   ref = "master";
  # };

  buildInputs = [
    youtube-dl ffmpeg # For youtube-mp3
    dmenu bluez # For blue
  ];

  installPhase = ''
    make DESTDIR="$out" PREFIX="" install
  '';
}
